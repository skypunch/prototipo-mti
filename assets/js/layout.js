'use strict';
(function () {
  // obtencion de main y header
  var main = window.document.getElementsByTagName('main')[0];
  var header = window.document.getElementById('header');
  // guardando las clases base del header
  header.basicClasses = header.className+' ';
  // oteniendo todos los elementos que desplieguen a otro (dropDowns)
  var dropDowns= window.document.getElementsByClassName('dropdown-menu');
  /*
  Esta funcion hace aparecer y desaparecer un elemento de dropdown
  */
  function activate(element){
    if (element) {
      if(element.hidden){
        element.className = element.basicClasses+'animated fadeInLeftBig duration-1';
        element.hidden=!element.hidden;
      }
      else{
        element.className = element.basicClasses+'animated fadeOutLeftBig duration-1';
        setTimeout(function () {
          element.hidden=!element.hidden;
          element.className+=' hidden';
        }, 550);
      }
    }
    else{
      console.error('Error al cargar');
    }
  };
  // se asigan la funcion activate a cada elemento con la clase dropdown
  var items = [];
  for (var i = 0; i < dropDowns.length; i++) {
    items[i] = document.getElementById(dropDowns[i].getAttribute('data-actives'))||window.document.getElementById(dropDowns[i].getAttribute('data-actives'));
    items[i].basicClasses=items[i].className+' ';
    dropDowns[i].item = items[i];
    dropDowns[i].onclick = function(){
      setTimeout(activate(this.item),500);
    };
    items[i].onclick = function(){
      activate(this);
    };
    items[i].close = function(){
      this.hidden=!this.hidden;
      this.className = this.basicClasses+'hidden';
    }
    items[i].close();
  };
  // al hacer scroll sobre el elemento principal se verifica el header fixed
  main.onscroll=function(){
    if(main.scrollTop>40){
      if (header.classList[header.classList.length-1]!='sticky') {
        header.className=header.basicClasses+'sticky';
      }
    }
    else{
      header.className=header.basicClasses;
    }
  };
  // funcion subir el scroll al tope superior de la pagina
  header.toTop=function(){
    main.scrollTop=0;
  };
})();
