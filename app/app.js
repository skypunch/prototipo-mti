'use strict';
(function(){
  angular.module('mti', ['ngRoute','ngAnimate','mtiControllers'])
    .config(['$routeProvider',function(router) {
      router.when('/', {
        templateUrl: 'app/views/home.html',
        controller: 'homeController',
        controllerAs: 'home'
      })
      .when('/conocenos', {
        templateUrl: 'app/views/about.html',
        controller: 'aboutController',
        controllerAs: 'about'
      })
      .when('/contactanos', {
        templateUrl: 'app/views/contact.html',
        controller: 'contactController',
        controllerAs: 'contact'
      })
      .when('/servicios', {
        templateUrl: 'app/views/services.html',
        controller: 'servicesController',
        controllerAs: 'services'
      })
      .when('/servicios/:type', {
        templateUrl: 'app/views/service.html',
        controller: 'serviceController',
        controllerAs: 'service'
      })
      .when('/recursos', {
        templateUrl: 'app/views/resources.html',
        controller: 'resourcesController',
        controllerAs: 'resources'
      })
      .when('/404',{
        templeteUrl: 'app/views/404.html'
      }).otherwise({
        redirectTo: '/404'
      });
    }]);
})();
