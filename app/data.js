
(function () {
  angular.module('siteData', [])
  .factory('homeData',['$rootScope','$http',function(scope,http){
    return function(){
      http.get('/app/data/home.json').then(function(res){
        scope.currentPage=res.data;
      });
      http.get('/app/data/slidesHome.json').then(function(res){
        scope.site.slides=res.data;
      });
    }
  }])
  .factory('aboutData',['$rootScope','$http',function(scope,http){
    return function(){
      http.get('/app/data/quienes.json').then(function(res){
        scope.currentPage=res.data;
      });
    }
  }])
  .factory('contactData',['$rootScope','$http',function(scope,http){
    return function(){
      http.get('/app/data/contacto.json').then(function(res){
        scope.currentPage=res.data;
        console.log(scope.currentPage);
      });
    }
  }])
  .factory('servicesData',['$rootScope','$http',function(scope,http){
    return function(){
      http.get('/app/data/servicios.json').then(function(res){
        scope.currentPage=res.data;
      });
    }
  }])
  .factory('serviceData',['$rootScope','$http',function(scope,http){
    return function(service) {
      http.get('/app/data/'+service.split('/')[2]+'.json').then(function(res){
        scope.currentPage=res.data;
      });
    }
  }])
  .factory('resourcesData',['$rootScope','$http',function(scope,http){
    return function(){
      http.get('/app/data/recursos.json').then(function(res){
        scope.currentPage=res.data;
      });
    }
  }])
  .factory('appData',['$rootScope','$http','dropdown-menu',function(scope,http,dropdownSet){
    return function(){
      var aux = {
        menu:{},
        submenus:[],
        map:{}
      };
      http.get('/app/data/menu.json').then(function(re){
        aux.menu=re.data;
      });
      http.get('/app/data/submenus.json').then(function(res){
        aux.submenus=res.data.elements;
      });
      http.get('/app/data/sitemap.json').then(function(r){
        aux.map=r.data;
      });
      setTimeout(function(){
        scope.slideContainer = window.document.getElementById('image-gradient');
        scope.slideImage = window.document.getElementById('dinamic-image');
        scope.slideContainer.basicClasses=scope.slideContainer.className+' ';
        scope.slideImage.basicClasses=scope.slideImage.className+' ';
        scope.slideContainer.className=scope.slideContainer.basicClasses+scope.slideContainer.getAttribute('data-animation-load');
        scope.site = scope.site || {
          "languaje":{
            "index":{
              "current":0,
              "next":1
            },
            "list":["Español","English"],
            "length":function(){
              return this.list.length;
            },
            "update":function(){
              this.next = this.list[this.index.next];
              this.current = this.list[this.index.current];
            },
            "change":function(){
              if(this.index.current<this.length()-1){
                this.index.current++;
              }
              else{
                this.index.current=0;
              }
              if(this.index.current<this.length()-1){
                this.index.next++;
              }
              else{
                this.index.next=0;
              }
              this.update();
              dropdownSet();
            },
            "current":"",
            "next":""
          },
          "menu":aux.menu,
          "submenus":aux.submenus,
          "map":aux.map
        }
        setTimeout(function () {
          dropdownSet();
        },500);
      },1500);
    }
  }])
  .factory('dropdown-menu',['$window','$timeout',function(window,timeout){
    return function(){
      timeout(function(){
        var dropDowns= window.document.getElementsByClassName('dropdown-submenu');
        dropDowns.basicClasses=dropDowns.className+' ';
        if (dropDowns.length>0) {
          function activate(element){
            if (element) {
              if(element.hidden){
                element.className = 'animated duration-1 '+element._in+' '+element._bg;
                element.hidden=!element.hidden;
              }
              else{
                element.className = 'animated duration-1 '+element._out+' '+element._bg;
                timeout(function () {
                  element.hidden=!element.hidden;
                  element.className+=' hidden';
                }, 550);
              }
            }
            else{
              console.error('Error al cargar');
            }
          };
          var items = [];
          for (var i = 0; i < dropDowns.length; i++) {
            items[i] = document.getElementById(dropDowns[i].getAttribute('data-actives'))||window.document.getElementById(dropDowns[i].getAttribute('data-actives'));
            items[i]._in = items[i].getAttribute('data-animation-in') || 'fadeInLeftBig';
            items[i]._out = items[i].getAttribute('data-animation-out') || 'fadeOutLeftBig';
            items[i]._bg = items[i].getAttribute('data-background') || 'black';
            items[i].basicClasses=items[i]._bg+' ';
            items[i].hidden=false;
            dropDowns[i].item = items[i];
            dropDowns[i].onclick = function(){
              setTimeout(activate(this.item),500);
            };
            items[i].onclick = function(){
              activate(this);
            };
            items[i].close = function(){
              this.hidden=!this.hidden;
              this.className = this.basicClasses+'hidden';
            }
            items[i].close();
          };
        }
      },100);
      return true;
    }
  }]);
})();
