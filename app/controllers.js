
(function(){
  angular.module('mtiControllers',['ngLocale','ngAria','siteData'])
  .controller('appController',['$rootScope','appData','$timeout','$location',function(scope,appData,timeout,location){
    appData();
    scope.url={
      encode:encodeURIComponent,
      decode:decodeURIComponent
    };
    scope.location=function(){
      return scope.url.encode(location.absUrl());
    };
    scope.slideAction=function(action) {
      $('.carousel').carousel(action)
    };
    timeout(function(){
      scope.site.languaje.update();
    },1800);
  }])
  .controller('homeController',['$rootScope','base','homeData','slider','$timeout',function(scope,run,homeData,slider,timeout){
    timeout(function() {
      homeData();
      timeout(function () {
        timeout(function(){
          run();
        },1200);
        scope.slider=slider(6000);
        scope.slider.start;
      },1500);
    },1800);
  }])
  .controller('aboutController',['$rootScope','$timeout','base','aboutData',function(scope,timeout,run,aboutData){
    timeout(function () {
      aboutData();
      timeout(function(){
        run();
      },1500);
    },2000);
  }])
  .controller('servicesController',['$rootScope','$timeout','base','servicesData',function(scope,timeout,run,servicesData){
    timeout(function () {
      servicesData();
      timeout(function(){
        run();
      },1500);
    },2000);
  }])
  .controller('serviceController',['$rootScope','$location','$timeout','base','serviceData',function(scope,location,timeout,run,servicesData){
    timeout(function () {
      servicesData(location.path());
      timeout(function(){
        run();
        $('.carousel').carousel({
          interval: 3000
        })
      },1500);
    },2000);
  }])
  .controller('contactController',['$rootScope','$http','$timeout','base','contactData',function(scope,http,timeout,run,contactData){
    timeout(function () {
      contactData();
      timeout(function(){
        run();
      },1500);
    },2000);
    var form = {
      nombre:"",
      empresa:"",
      email:"",
      telefono:"",
      asunto:"",
      mensaje:"",
      check:false
    };
    this.form = form;
    this.reset=function(){
      this.form={
        nombre:"",
        empresa:"",
        email:"",
        telefono:"",
        asunto:"",
        mensaje:"",
        check:false
      };
    };
    this.send=function(){
      var toSend=this.form;
      if(toSend.check){
        var pack={
          "text":    toSend.mensaje+"\nnombre:"+toSend.nombre+"\nempresa:"+toSend.empresa+"\ntelefono: "+toSend.telefono+"\nemail: "+toSend.email,
          "from":    "No Responder <no_reply_skypunch@outlook.com>",
          "to":      "Ricardo <ingrnl23@gmail.com>",
          "subject": toSend.asunto
        };
        http.post('https://mail-services.azurewebsites.net/mailer',pack)
        .then(function(res){
          alert("Envio Correcto.\nGracias por su mensaje");
        }, function(res){
          alert("Se produjo un error, por favor intentelo mas tarde");
        });
        this.reset();
      }
      else{
        alert("No acepto los terminos in condiciones")
      }
    };
    scope.setGalery=function(prefix){
      scope.sucursal=prefix;
      scope.page.toTop();
      scope.original=scope.page.main.className;
      scope.page.main.className=scope.page.main.className+" block-scroll";
      timeout(function(){
          scope.page.main.className=scope.original;
      },4000)
    }
  }])
  .controller('resourcesController',['$rootScope','$timeout','base','resourcesData',function(scope,timeout,run,resourcesData){
    timeout(function () {
      resourcesData();
      timeout(function(){
        run();
      },1500);
    },2000);
  }])
  .factory('slider',['$rootScope','$interval','$timeout','$window',function(scope,interval,timeout,window){
    return function(timer){
      var slider = {
        timer: timer || 6000,
        index:0,
        length: function(){
          return scope.site.slides[scope.site.languaje.current].length;
        },
        position: function(index){
          if(index || index===0){
            this._disable();
            if (index < this.length()) {
              this.index = index;
            }
            else{
              this.index = 0;
            }
            this.stop();
            this.start;
          }
          this._enable();
          scope.slide = scope.site.slides[scope.site.languaje.current][this.index];
          scope.slide.index=this.index;
          return scope.slide;
        },
        _enable:function(){
          scope.site.slides[scope.site.languaje.current][this.index].class="active";
        },
        _disable:function(){
          scope.site.slides[scope.site.languaje.current][this.index].class=""
        },
        next:function(reset){
          reset = reset || false;
          scope.slideImage.className=scope.slideImage.basicClasses+scope.slideImage.getAttribute('data-animation-in')+' duration-1';
          timeout(function(){
            scope.slideImage.className=scope.slideImage.basicClasses+scope.slideImage.getAttribute('data-animation-out')+' duration-1';
          },this.timer-1000);
          this._disable();
          if(this.index<this.length()-1){
            this.index++;
          }
          else{
            this.index = 0;
          }
          if(reset){
            this.stop();
            this.start;
          }
          return this.position();
        },
        prev:function(reset){
          reset = reset || false;
          this._disable();
          if(this.index===0){
            this.index = this.length() - 1;
          }
          else{
            this.index--;
          }
          if(reset){
            this.stop();
            this.start;
          }
          return this.position();
        }
      };
      slider.start=interval(function(){
        slider.next();
      },slider.timer),
      slider.stop=function(){
        clearInterval(slider.start);
      }
      slider.position();
      return slider;
    };
  }])
  .factory('base',['$window','$rootScope',function(window,scope){
    return function() {
      scope.page = scope.page || {};
      scope.page.header=window.document.getElementById('header');
      scope.page.main = window.document.getElementsByTagName('main')[0];
      scope.page.main.scrollTop=0;
      scope.page.header.basicClasses = scope.page.header.basicClasses || scope.page.header.className+' ';
      scope.page.header.sticky=false;
      scope.page.header.className=scope.page.header.basicClasses;
      scope.page.toTop=function(){
        scope.page.main.scrollTop=0;
      };
      scope.page.toTop();
    };
  }]);
})();
